# Django Web-App: Event RSVP System

The event RSVP system helps users to manage events as different roles(owner,vendor,guest). Owners will sent invites to guests and add vendors to the event created, guests will respond to invitation and then conduct a survey for vendors to check. The database we use is PostgreSQL, and we implemented on Django platform(2.0.2).

## Getting Started

These instructions will run the project on your local machine. 

### Prerequisites

Django 2.0.2
PostgreSQL
Python 3

### Functionalities

This app is able to support a series of functionality and features as an RSVP system, the details are listed and explained as following:

1. Create Account: go to 127.0.0.0:8000/account/register, register requires an email address. This address will be used for receiving invitations and updates about the survey questions. 
2. Event Creation: every user can create his or her own event by clicking then "create event" link at the bottom of user homepage. Inside the link, user can choose the event name, date, description and plus-one permission for the creating event.
3. Event Management: in user homepage, user can check all the events he/she participates and categorized to 3 different roles. 

Managing events as owner, one can add owner, add vendors, add guests and send invitations. By clicking the vendor name in vendorList at your owner_event_page, you can edit question you want to send to the guest and give control and permission to that vendor.This vendor will be able to finalize this question at his vendor_event_page. If owner edits survey questions, guests will receive an email notification

Managing events as vendor, one can finalize question which owner gives permission to, and check answers given by guests.

Managing events as guest, one can decide if he/she accepts/declines the invitation. And fill the number of people he/she wants to bring with if this event allows plus-one. If the guest decides to attend the event, he/she will have to answer several questions(free-text or multiple choice question) and the answer can be edited until vendor finalizes it.

## Acknowledgments

* https://docs.djangoproject.com/en/2.0/

